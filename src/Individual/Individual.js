import React, {useState} from 'react';
import {nanoid} from "nanoid";
import './Individal.css';

const Individual = () => {
    const [people, setPeople] = useState([]);

    const [addInf, setAddInf] = useState({
        percent: 0,
        delivery: 0
    });

    const addPerson = () => {
        setPeople(people => [...people, {name: '', price: '', personService: 0, id: nanoid()}]);
    };

    const changePersonField = (id, name, value) => {
        if (name === 'price') {
            setPeople(people => {
                return people.map(person => {
                    if (person.id === id) {
                        return {...person, [name]: parseInt(value) || 0}
                    }

                    return person;
                });
            });
        } else {
            setPeople(people => {
                return people.map(person => {
                    if (person.id === id) {
                        return {...person, [name]: value}
                    }

                    return person;
                });
            });
        }
    };

    const removePerson = id => {
        setPeople(people.filter(p => p.id !== id));
    };

    const onAddInputChange = e => {
        const value = parseInt(e.target.value);
        const name = e.target.name;

        setAddInf(prev => ({
            ...prev,
            [name]: value || ''
        }));
    };

    const [total, setTotal] = useState(0);

    const getInfo = e => {
        e.preventDefault();

        let total = 0;

        people.forEach(person => {
            total = total + person.price

        });

        const tips = ((total / 100) * addInf.percent) + total + addInf.delivery;

        setTotal(tips);

        const personDelivery = addInf.delivery / people.length;

        setPeople(people.map(person => {
            return {
                ...person,
                personService: Math.ceil(person.price + ((person.price / 100) * addInf.percent)) + personDelivery
            };
        }));
    };

    return (
        <div>
          <form onSubmit={getInfo}>
              {people.map(person => (
                  <div key={person.id} style={{marginBottom: '15px'}}>
                      <input
                          type="text"
                          placeholder="Name"
                          value={person.name}
                          onChange={e => changePersonField(person.id, 'name', e.target.value)}
                      />
                      <input
                          type="number"
                          placeholder="Sum"
                          value={person.price}
                          onChange={e => changePersonField(person.id, 'price', e.target.value)}
                      />
                      <button type="button" onClick={() => removePerson(person.id)}>Remove</button>
                  </div>
              ))}

              <button type="button" onClick={addPerson} className="add-btn">+</button>

              <div className="form-row">
                  <label>
                      Процент чаевых:
                      <input
                          type="number"
                          className="formField"
                          value={addInf.percent}
                          name="percent"
                          onChange={onAddInputChange}
                      />
                      %
                  </label>
              </div>
              <div className="form-row">
                  <label>
                      Доставка:
                      <input
                          type="number"
                          className="formField"
                          value={addInf.delivery}
                          name="delivery"
                          onChange={onAddInputChange}
                      />
                      сом
                  </label>
              </div>
              <button type="submit">Расчитать</button>
          </form>
            <div>
                <p>Общая сумма: {total}</p>
                {people.map(person => (
                    <p key={person.id}>{person.name}: {person.personService}</p>
                ))}
            </div>
        </div>
    );
};

export default Individual;