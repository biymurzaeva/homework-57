import React, {useState} from 'react';
import './PaymentTypes.css';
import Split from "../Split/Split";
import Individual from "../Individual/Individual";

const PaymentTypes = () => {
    const [mode, setMode] = useState('individual');

    const onRadioChange = e => {
        setMode(e.target.value);
    };

    return (
        <>
            <div className="types">
                <form>
                    <p>Сумма заказа считается:</p>
                    <div className="radio">
                        <label>
                            <input
                                type="radio"
                                name="mode"
                                value="split"
                                checked={mode === 'split'}
                                onChange={onRadioChange}
                            />
                            Поровну между всеми участниками
                        </label>
                    </div>
                    <div className="radio">
                        <label>
                            <input
                                type="radio"
                                name="mode"
                                value="individual"
                                checked={mode === 'individual'}
                                onChange={onRadioChange}
                            />
                            Каждому индивидуально
                        </label>
                    </div>
                </form>
            </div>
            <div>
                {mode === 'split' ? (
                    <Split/>
                ) : (
                    <Individual/>
                )}
            </div>
        </>
    );
};

export default PaymentTypes;