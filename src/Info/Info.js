import React from 'react';

const Info = props => {
    return (
        <>
            <p>Общая сумма: <strong>{props.total}</strong> сом</p>
            <p>Количество человек: <strong>{props.numberOfPerson}</strong></p>
            <p>Каждый платит по: <strong>{props.everyPay}</strong> сом</p>
        </>
    );
};

export default Info;