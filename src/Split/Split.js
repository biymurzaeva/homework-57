import React, {useState} from 'react';
import './Split.css';
import Info from "../Info/Info";

const Split = () => {
    const [splitInfo, setSplitInfo] = useState({
        count: 0,
        orderAmount: 0,
        tip: 0,
        delivery: 0
    });

    const onInputChange = e => {
        const value = parseInt(e.target.value);
        const name = e.target.name;

        setSplitInfo(prev => ({
            ...prev,
            [name]: value || ''
        }));
    };

    const [info, setInfo] = useState({
        count: 0,
        sum: 0,
        everyPerson: 0
    });

    const calculate = e => {
        e.preventDefault();

        const total = splitInfo.orderAmount + ((splitInfo.orderAmount / 100) * splitInfo.tip) + splitInfo.delivery;

        setInfo({
            count: splitInfo.count,
            sum: total,
            everyPerson: Math.ceil(total / splitInfo.count),
        });
    };

    return (
        <div>
            <form onSubmit={calculate}>
                <div className="form-row">
                    <label>
                        Человек:
                        <input
                            type="number"
                            className="formField"
                            value={splitInfo.count}
                            name="count"
                            onChange={onInputChange}
                        />
                        чел.
                    </label>
                </div>
                <div className="form-row">
                    <label>
                        Сумма заказа:
                        <input
                            type="number"
                            className="formField"
                            value={splitInfo.orderAmount}
                            name="orderAmount"
                            onChange={onInputChange}
                        />
                        сом
                    </label>
                </div>
                <div className="form-row">
                    <label>
                        Процент чаевых:
                        <input
                            type="number"
                            className="formField"
                            value={splitInfo.tip}
                            name="tip"
                            onChange={onInputChange}
                        />
                        %
                    </label>
                </div>
                <div className="form-row">
                        <label>
                            Доставка:
                            <input
                                type="number"
                                className="formField"
                                value={splitInfo.delivery}
                                name="delivery"
                                onChange={onInputChange}
                            />
                            сом
                        </label>
                    </div>
                <button type="submit">Расчитать</button>
            </form>
            <Info
                total={info.sum}
                numberOfPerson={info.count}
                everyPay={info.everyPerson}
            />
        </div>
    );
};

export default Split;